from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner


class MyComponent(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
        def onevent(msg):
            print("Wiadomosc: {}".format(msg))
        yield self.subscribe(onevent, u'py.kato.crossbar')


if __name__ == '__main__':
    runner = ApplicationRunner(
        u"ws://127.0.0.1:8080/ws",
        u"realm1",
    )
    runner.run(MyComponent)