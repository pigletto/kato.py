kato.py 21.06.2016
------------------

Presentation: **Crossbar.io - WAMP Router**

This repository contains code used during presentation.
In order to run the samples it is required to install and start crossbar:

    $ mkvirtualenv kato.py.crossbar
    $ pip install crossbar
    $ cd crossbar_1/
    $ crossbar start
    
Folder ***crossbar_1/*** 

    .crossbar/        # default configuration of crossbar.io
    autobahn.min.js   # WAMP client library for JS
    crossclient.html  # simple client implementation in HTML/JS (publish/subscribe) 
    crossclient.py    # simple client implementation in Python (subscribe)
    style.css         # just styles for crossclient.html
     
Folder ***crossbar_2***

Contains same files as **crossbar_1** but crossbar configuration in **.crossbar/** is updated with:
 
   * **publisher** - makes it possible to publish messages using POST request (sample implementation in **publish_message.py**)
   * **component hosting** - **crossclient.py** is run within crossbar.io 

